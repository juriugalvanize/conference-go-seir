import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


while True:
    try:

        def process_approval(ch, method, properties, body):
            print("Approval Fired")
            content = json.loads(body)
            name = content.get("presenter_name")
            email = content.get("presenter_email")
            title = content.get("title")

            send_mail(
                "Presentation Approved!",
                f"Hello {name}, your presentation - {title} has been approved!",
                "admin@conference.go",
                [email],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            print("Rejection Fired")
            content = json.loads(body)
            name = content.get("presenter_name")
            email = content.get("presenter_email")
            title = content.get("title")

            send_mail(
                "Presentation Rejected!",
                f"Hello {name}, your presentation - {title} has been rejected.",
                "admin@conference.go",
                [email],
                fail_silently=False,
            )

        def main():
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.queue_declare(queue="approvals")
            channel.queue_declare(queue="rejections")
            channel.basic_consume(
                queue="approvals",
                on_message_callback=process_approval,
                auto_ack=True,
            )
            channel.basic_consume(
                queue="rejections",
                on_message_callback=process_rejection,
                auto_ack=True,
            )

            print(" [*] Waiting for messages. To exit press CTRL+C")

            channel.start_consuming()

        # excude code when the file runs as a script, but not when its imported as a module
        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:  # allows for a keyboard interrupt ("Ctrl + C")
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)

import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from datetime import datetime


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


def update_account(ch, method, properties, body):
    print("Account active")
    content = json.loads(body)
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    content["updated"] = datetime.fromisoformat(updated_string)
    if is_active:
        AccountVO.objects.update_or_create(**content)
    else:
        AccountVO.objects.filter(email=email).delete()


def main():
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="account_info", exchange_type="fanout")
    result = channel.queue_declare(
        queue="", exclusive=True
    )  # What does this do?
    queue_name = result.method.queue  # What does this do?
    channel.queue_bind(exchange="account_info", queue=queue_name)
    channel.basic_consume(
        queue=queue_name, on_message_callback=update_account, auto_ack=True
    )
    channel.start_consuming()
    print(" [*] Waiting for messages. To exit press CTRL+C")


while True:
    try:
        # excude code when the file runs as a script, but not when its imported as a module
        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:  # allows for a keyboard interrupt ("Ctrl + C")
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
